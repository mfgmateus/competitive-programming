fibs = []
sums = []
fibs.append(0)
fibs.append(1)
fibs.append(1)
sums.append(0)
sums.append(1)
sums.append(2)
k = len(sums) - 1
n = int(input())
while n > 0:
    a, b = input('').split(' ')
    a = int(a)
    b = int(b)
    while k <= b:
        i = k%150
        if i != 0:
            s = fibs[i] + fibs[i-1]
        else:
            s = 0
        if s > 99:
            s = s%100
        fibs.append(s)
        sums.append(sums[k] + s)
        k = len(sums) - 1
    s = sums[b] - sums[a-1]
    print(fibs)
    print(sums)
    print(s)
    n -= 1
