from math import tan, radians
l, h, w, tet = map(int, input().split(' '))
vp = ((l**2*tan(radians(tet))) * w)/2
print("%.3f ml" % ((l*w*h)-(vp)))
