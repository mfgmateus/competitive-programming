#include <iostream>
#include <string>

using namespace std;

int main(){
    string::npos;
    int nc;
    cin >> nc;
    while(nc >0){
        string s = "";
        cin >> s;
        while(s.find(".") != string::npos){
            s.replace(s.find("."),1,"");
        }
        int c = 0;
        while(s.find("<>") != string::npos){
            s.replace(s.find("<>"),2,"");
            c++;
        }
        cout << c << std::endl;
        nc--;
    }
}
