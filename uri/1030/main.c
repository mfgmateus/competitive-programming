#include <stdio.h>
#include <stdlib.h>
struct Node{
    int key;
    struct Node* next;
};
typedef struct Node Node;
Node* newNode(int val){
    Node* node = malloc(sizeof(Node));
    node->key = val;
    return node;
}
void printNodes(Node* first, int n){
    Node* p = first;
    int i;
    for(i = 0; i < n; i++){
        printf("%d ",p->key);
        p = p->next;
    }
    printf("\n");
}
int main(){
    int nc,i,c;
    scanf("%d",&nc);
    for(c = 0; c < nc; c++){
        int n, k,j;
        scanf("%d %d",&n,&k);
        Node* first = newNode(1);
        Node* current = first;
        for(j = 2; j <= n; j++){
            current->next = newNode(j);
            current = current->next;
        }
        current->next = first;
        int r = n;
        first = current;
        while(first != first->next){
            for(i = 1; i < k; i++)
                first = first->next;
            r--;
            Node* temp = first->next;
            first->next = first->next->next;
            free(temp);
        }
        printf("Case %d: %d\n",c+1,first->key);
    }
    return 0;
}   
