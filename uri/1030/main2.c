#include <stdio.h>
int j(int n, int k){
    if(n == 1)
        return 1;
    return (j(n-1,k)+k-1)%n+1;
}
int main(){
    int nCases,i;
    scanf("%d",&nCases);
    for(i = 1; i <= nCases; i++){
        int n, k;
        scanf("%d %d",&n,&k);
        printf("Case %d: %d\n",i,j(n,k));
    }
}
