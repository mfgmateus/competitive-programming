cases = input()
cont = 1
while cases > 0:
    p, t = raw_input('').split(' ')
    f = []
    p = int(p)
    t = int(t)
    for i in range(1,p+1):
        f.append(i)
    r = 0
    t = t-1
    while len(f) > 1:
        r = (r + t)
        if r >= len(f):
            r = (r % len(f))
        del f[r]
    print "Case %d: %d" % (cont,f[0])
    cont += 1
    cases -= 1
