#include <stdio.h>
int mdc(int a, int b){
    if(b == 0)
        return a;
    else
        return mdc(b, a%b);

}
void change(int *a, int* b){
    if(*a > *b){
       int temp = *a;
       *a = *b;
       *b = temp;
    }
}
int main(){
    int nc;
    scanf("%d",&nc);
    while(nc){
        int a,b;
        scanf("%d %d",&a,&b);
        change(&a,&b);
        printf("%d\n",mdc(a,b));
        nc--;
    }
    return 0;
}
