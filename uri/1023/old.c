#include <stdio.h>
#include <stdlib.h>

struct Node{
    int nPessoas;
    int consumo;
    int media;
    struct Node* next;
};

typedef struct Node Node;

struct LinkedList{
    int size;
    Node* head;
    int somaConsumo;
    int somaPessoas;
};

typedef struct LinkedList LinkedList;

Node* newNode(int nPessoas, int nConsumo){
    Node* consumo = malloc(sizeof(Node));
    consumo->nPessoas = nPessoas;
    consumo->consumo = nConsumo;
    consumo->media = nConsumo / nPessoas;
}
LinkedList* newList(){
    LinkedList* list = malloc(sizeof(LinkedList));
    list->size = 0;
    list->head = NULL;
    list->somaConsumo = 0;
    list->somaPessoas = 0;
    return list;
}

void appendList(LinkedList* list, Node* consumo){

    if(list->head == NULL)
        list->head = consumo;
    else{
        if(list->head->media > consumo->media){
            consumo->next = list->head;
            list->head = consumo;
        }else{
            Node* pointer = list->head;
            while(pointer->next->media < consumo->media){
                if(pointer->next->next == NULL) {
                    pointer = pointer->next;
                    break;
                }
                pointer = pointer->next;
            }
            consumo->next = pointer->next;
            pointer->next = consumo;
        }
    }
    list->somaConsumo += consumo->consumo;
    list->somaPessoas += consumo->nPessoas;
    list->size++;
} 
void printList(LinkedList* list){
    Node* pointer = list->head;
    while(pointer != NULL){
        printf("%d-%d",pointer->nPessoas, pointer->media);
        pointer = pointer->next;
        if(pointer != NULL) printf(" ");
    }
    printf("\nConsumo medio: %.2lf m3.\n",(double)list->somaConsumo/(double)list->somaPessoas);
}

int main(){
    
    int nCases, i= 0, counter = 1;
    scanf("%d",&nCases);
    int spaco = 0;

    while(nCases != 0){
        LinkedList* lista = newList();
        i = nCases;
        while(i > 0){
            int pessoas, consumo;
            scanf("%d %d",&pessoas,&consumo);
            appendList(lista, newNode(pessoas,consumo));
            i--;
        }
        if(!spaco) spaco++;
        else printf("\n");
        printf("Cidade# %d:\n",counter);
        printList(lista);
        counter++;
        scanf("%d",&nCases);
    }
}
