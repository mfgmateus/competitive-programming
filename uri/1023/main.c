#include <stdio.h>
#include <stdlib.h>

int main(){
    
    int nCases, i= 0, counter = 1;
    scanf("%d",&nCases);
    int spaco = 0;

    while(nCases != 0){
        i = nCases;
        
        int nPessoas=0;
        int soma=0;

        int lista[202];
        int j;
        for(j = 0; j <= 202; j++){
            lista[j] = 0;
        }   
        while(i > 0){
            int pessoas, consumo;
            scanf("%d %d",&pessoas,&consumo);
            int key = (int)consumo/(int)pessoas;
            lista[key] += pessoas;
            nPessoas += pessoas;
            soma += consumo;
            i--;
        }
        if(!spaco) spaco++;
        else printf("\n");
        printf("Cidade# %d:\n",counter);
        int sp=0;
        for(j = 0; j < 203; j++){
            if(lista[j] > 0){
                if(sp) printf(" ");
                else sp++;
                printf("%d-%d",lista[j],j);
            }
        }
        printf("\n");
        double media = (double)soma/nPessoas;
        char buf[48];
        sprintf(buf,"%lf",media);
        for(j = 0; j < 48; j++)
            if(buf[j] == '.') buf[j+3] = '\0';
        printf("Consumo medio: %s m3.\n",buf);
        counter++;
        scanf("%d",&nCases);
    }
}
