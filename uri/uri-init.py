from bs4 import BeautifulSoup
import sys,os
import requests

if(not os.path.exists(sys.argv[1])):
	os.makedirs(sys.argv[1])
url = "https://www.urionlinejudge.com.br/repository/UOJ_"+sys.argv[1]+".html"
r = requests.get(url, verify=False)
doc = r.content
desc = ' '.join(BeautifulSoup(doc,'html.parser').findAll("div", { "class" : "description" })[0].find('p').text.lstrip().split()).encode('utf-8')
ent = ' '.join(BeautifulSoup(doc,'html.parser').findAll("div", { "class" : "input" })[0].find('p').text.lstrip().split()).encode('utf-8')
saida = ' '.join(BeautifulSoup(doc,'html.parser').findAll("div", { "class" : "output" })[0].find('p').text.lstrip().split()).encode('utf-8')
in1 =  ' '.join(BeautifulSoup(doc,'html.parser').findAll("td", { "class" : "division" })[0].parent.findAll('td')[0].text.replace('\n',';').split()).replace(";;","").replace(";",'\n');
out1 =  ' '.join(BeautifulSoup(doc,'html.parser').findAll("td", { "class" : "division" })[0].parent.findAll('td')[1].text.replace('\n',';').split()).replace(";;","").replace(";",'\n');

c = sys.argv[1]+"/"+sys.argv[1]+".c"
if(os.path.isfile(c)):
	os.rename(c, c+".old")
f = open(c,'w')
f.write("#include <stdio.h>\n")
f.write("/*\n")
f.write("\n#Description\n"+desc+"\n")
f.write("\n#Input\n"+ent+"\n")
f.write("\n#Output\n"+saida+"\n")
f.write("\n#Example\n"+in1+"\n")
f.write("\n------------------------------------------------------------\n")
f.write("\n"+out1+"\n")
f.write("*/\n")
f.write("int main(){\n")
f.write("\t\n")
f.write("\treturn 0;\n")
f.write("}\n")
f.close()
