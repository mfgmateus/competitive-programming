#include <iostream>
#include <stack>
using namespace std;
int main(){
    string e, ef;
    while(cin >> e){
        stack<char> pilha;
        ef = "";
        string temp2 = "";
        bool empilha = false;
        for(int i = 0; i < e.size(); i++){ 
            if(e[i] == ']'){
                string temp;
                while(!pilha.empty() && pilha.top() != '['){
                    temp = pilha.top() + temp;
                    pilha.pop();
                }
                if(!pilha.empty()) pilha.pop();
                ef = temp + ef;
                empilha = false;
            }else if(e[i] == '[') {
                temp2 = "";
                while(!pilha.empty()){
                    temp2 = pilha.top() + temp2;
                    pilha.pop();
                }
                ef = temp2 + ef;
                empilha = true;
            }else{
                if(empilha) pilha.push(e[i]);
                else ef += e[i];
            }

        }
        temp2 = "";
        while(!pilha.empty()){
            temp2 = pilha.top() + temp2;
            pilha.pop();
        }
        ef = temp2 + ef;
        cout << ef << endl;
    }
    return 0;
}
