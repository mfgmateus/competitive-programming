#include <stdio.h>
struct time{
    int pontos;
    int marcados;
    int recebidos;
    int inscricao;
};
typedef struct time time;
double getAverage(time* t){
    if(t->recebidos == 0) return (double)t->marcados;
    return (double)((double)t->marcados/(double)t->recebidos); 

}
void changeTimes(time* a, time *b){
    time temp = *a;
    *a = *b;
    *b = temp;
}
int main(){
    time tDefault;
    tDefault.pontos = 0;
    tDefault.marcados = 0;
    tDefault.recebidos = 0; 
    int nTimes;
    int inst = 1;
    scanf("%d",&nTimes);
    int nl = 0;
    while(nTimes != 0){
        int i;
        time times[nTimes];
        for(i = 0; i < nTimes; i++){
            times[i] = tDefault;
        }
        int cont = nTimes*(nTimes-1)/2;
        while(cont > 0){
            int t1, t2, c1,c2;
            scanf("%d %d %d %d",&t1,&c1,&t2,&c2);
            times[t1-1].inscricao = t1;
            times[t2-1].inscricao = t2;
            times[t1-1].marcados += c1;
            times[t1-1].recebidos += c2;
            times[t2-1].marcados += c2; 
            times[t2-1].recebidos += c1;
            if(c1 > c2){
                times[t1-1].pontos += 2;
                times[t2-1].pontos += 1;
            }else{
                times[t2-1].pontos += 2;
                times[t1-1].pontos += 1;
            }
            cont--;
        } 
        int j = 0;
        for(i = 0; i < nTimes; i++){
            for(j =0; j < nTimes; j++){
                if(i != j){
                    if(times[i].pontos == times[j].pontos){
                        double averageI = getAverage(&times[i]);
                        double averageJ = getAverage(&times[j]);
                        if(averageI == averageJ){
                            if(times[i].marcados == times[j].marcados){
                                if(times[i].inscricao > times[j].inscricao){
                                    changeTimes(&times[i],&times[j]);
                                }
                            }else if(times[i].marcados > times[j].marcados){
                                changeTimes(&times[i],&times[j]);
                            }
                        }else if(averageI > averageJ){
                            changeTimes(&times[i],&times[j]);
                        }

                    }else if(times[i].pontos > times[j].pontos){
                        changeTimes(&times[i],&times[j]);
                    }
                }
            }
        }
        if(nl) printf("\n");
        else nl++;
        printf("Instancia %d\n",inst++);
        int sp = 0;
        for(i = 0; i < nTimes; i++){
            if(sp) printf(" ");
            else sp++;
            printf("%d",times[i].inscricao);
        }
        printf("\n");
        scanf("%d",&nTimes);
    }
    int i =0;
    return 0;
}
