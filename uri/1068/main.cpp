#include <iostream>
using namespace std;

int main() {
	string entrada = "";
	while(cin >> entrada){
		string e = "";
		for(int i = 0; i < entrada.size(); i++){
			if(entrada.at(i) == ')' || entrada.at(i) == '('){
				e += entrada.at(i);
			}
		}
		while(e.find("()") != string::npos){
	            e.replace(e.find("()"),2,"");
	        }
	    if(e != "") cout << "incorrect" << endl;
	    else cout << "correct" << endl;
	}
	return 0;
}
