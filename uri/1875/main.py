def getPoints(m,c):
    p = 0
    if m == 'R' and c == 'G':
        return 2
    if m == 'R' and c == 'B':
        return 1
    if m == 'G' and c == 'B':
        return 2
    if m == 'G' and c == 'R':
        return 1
    if m == 'B' and c == 'R':
        return 2
    if m == 'B' and c == 'G':
        return 1
    return 0
n = int(input())
while n > 0:
    g = int(input())
    R = 0
    G = 0
    B = 0
    while g > 0:
        m, c = input().split(' ')
        if m == 'R':
            R += getPoints(m,c)
        elif m == 'G':
            G += getPoints(m,c)
        elif m == 'B':
            B += getPoints(m,c)
        g -= 1
    if R == G and G == B:
        print('trempate')
    elif R > G and R > B:
        print('red')
    elif G > R and G > B:
        print('green')
    elif B > R and B > G:
        print('blue')
    elif R == G or G == B or R == B:
        print('empate')
    n -= 1
