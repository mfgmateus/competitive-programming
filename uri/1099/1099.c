#include <stdio.h>

int main(){
	int d,i;
	scanf("%d",&d);
	int v[d];
	for(i = 0; i < d; i++){
		int v1,v2;
		scanf("%d %d",&v1,&v2);
		if(v1 > v2){
			int temp = v1;
			v1 = v2;
			v2 = temp;
		}
		v1 += 1;
		v[i] = 0;
		for(;v1 < v2; v1++){
			if((v1 % 2) != 0) v[i] += v1;
		}
		
	}
	for(i = 0; i < d; i++){
		printf("%d\n",v[i]);		
	}
}
