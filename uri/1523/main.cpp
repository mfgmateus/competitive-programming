#include <iostream>
#include <stack>
using namespace std;

class motorista{
  public:
    int entrada;
    int saida;
};

int main() {
  int N, K;
  while(cin >> N >> K){
    if(N == 0) return 0;
    stack<motorista> estacionamento;
    bool possible = true;
    for(int i = 0; i < N; i++){
      motorista motorista;
      cin >> motorista.entrada >> motorista.saida;
      if(motorista.saida < motorista.entrada) possible = false;
      if(estacionamento.empty()){
        estacionamento.push(motorista);
      }else{
        while(!estacionamento.empty() && estacionamento.top().saida <= motorista.entrada){
          estacionamento.pop();
        }
        estacionamento.push(motorista);
        if(estacionamento.size() >= N) possible = false;
      }
    }
    motorista s = estacionamento.top();
    while(!estacionamento.empty() && estacionamento.top().saida >= s.saida){
      s = estacionamento.top();
      estacionamento.pop();
    }
    if(possible && estacionamento.empty()) cout << "Sim" << endl;
    else cout << "Nao" << endl;
  }
  return 0;
}
