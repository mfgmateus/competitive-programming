#include <stdio.h>

int fat(int v){
	if(v == 0) return 1;
	return v * fat(v-1);
}

int main(){
	int d;
	scanf("%d",&d);
	printf("%d\n",fat(d));
}
