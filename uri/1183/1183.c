#include <stdio.h>
/*

#Description
Leia um caractere maiúsculo, que indica uma operação que deve ser realizada e uma matriz M[12][12]. Em seguida, calcule e mostre a soma ou a média considerando somente aqueles elementos que estão acima da diagonal principal da matriz, conforme ilustrado abaixo (área verde).

#Input
A primeira linha de entrada contem um único caractere Maiúsculo O ('S' ou 'M'), indicando a operação (Soma ou Média) que deverá ser realizada com os elementos da matriz. Seguem os 144 valores de ponto flutuante que compõem a matriz.

#Output
Imprima o resultado solicitado (a soma ou média), com 1 casa após o ponto decimal.

#Example
 S
 1.0
 0.0
 -3.5
 2.5
 4.1
 ...


------------------------------------------------------------

 12.6

*/
int main(){
	float s=0,v;
    int i,j,d=0;
    char c;
    scanf("%c",&c);
    for(i = 0; i < 12; i++){
        for(j = 0; j < 12; j++){
            scanf("%f",&v);
            if(j > i){ 
				s+= v;
                d++;
            }
        }
    }
    if(c == 'M')
        s = s/d;
    printf("%.1f\n",s);
 
    return 0;
}
