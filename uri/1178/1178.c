#include <stdio.h>
/*

#Description
Leia um valor X. Coloque este valor na primeira posição de um vetor N[100]. Em cada posição subsequente de N (1 até 99), coloque a metade do valor armazenado na posição anterior, conforme o exemplo abaixo. Imprima o vetor N.

#Input
A entrada contem um valor de dupla precisão com 4 casas decimais.

#Output
Para cada posição do vetor N, escreva "N[i] = Y", onde i é a posição do vetor e Y é o valor armazenado naquela posição. Cada valor do vetor deve ser apresentado com 4 casas decimais.

#Example

 
 200.0000


------------------------------------------------------------


 
 N[0] = 200.0000 
 N[1] = 100.0000 
 N[2] = 50.0000 
 N[3] = 25.0000 
 N[4] = 12.5000 
 ...

*/
int main(){
    double f;
    int i;
    scanf("%lf",&f);
    for(i = 0; i < 100; i++){
        printf("N[%d] = %.4lf\n",i,f);
        f = f/2.0;
    }    
	return 0;
}
