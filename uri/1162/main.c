#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int sort(int *vet, int size);
int main(){

    int cases;
    scanf("%d",&cases);
    while(cases > 0){
        int i,j, size;
        int *v;
        scanf("%d", &size);
        v = malloc(size * sizeof(int));
        for(i=0; i < size; i++)
            scanf("%d", &v[i]);
        int swaps = 0;
        printf("Optimal train swapping takes %d swaps.\n",sort(v,size));
        cases--;
        free(v);
    }
    return 0;
}
int sort(int *vet, int size) {
    int i,j,sum=0;
    for(i = 0; i < size; i++){
        for(j = 0; j < size-1; j++){
            if(vet[j] > vet[j+1]){
                int temp = vet[j];
                vet[j] = vet[j+1];
                vet[j+1] = temp;
                sum++;
            }
        }
    }
    return sum;
}
