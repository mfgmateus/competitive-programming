#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

bool validaSalto(int n, int k){
    vector<int> p;
    for(int i = 0; i < n; i++){
        p.push_back(i+1);
    }
    int c = 0;
    while(p[c] != 13){
        p.erase(p.begin()+c);
        c = (c + k -1) % p.size();
    }
    return p.size() == 1;
}

int main(){
    int n,i,c; 
    while(cin >> n){
        if(n == 0) return 0;
        int k = 1;
        while(!validaSalto(n,k)){
            k++;
        }
        cout << k << endl;
    }
    return 0;
}   
