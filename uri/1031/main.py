def verificaSalto(N,m):
    lista = [i for i in range(1,N+1)]
    c = 0
    while(lista[c] != 13):
        del lista[c]
        c = (c + m - 1)%len(lista)
    return len(lista) == 1
N = int(input())
while N != 0:
    m = 1
    while not verificaSalto(N,m):
        m += 1
    print(m)
    N = int(input())
