#include <stdio.h>
#include <stdlib.h>
struct Node{
    int val;
    struct Node* right;
    struct Node* left;
    struct Node* next;
};
typedef struct Node Node;
struct BinaryTree{
    Node* root; 
};
typedef struct BinaryTree BinaryTree;
Node* newNode(int val){
    Node* n = malloc(sizeof(Node));
    n->right = NULL;
    n->left = NULL;
    n->val = val;
    return n;
}
BinaryTree* newTree(){
    BinaryTree* bt = malloc(sizeof(BinaryTree));
    bt->root = NULL;
    return bt;
}
void addNode(Node* parent, Node* n){
    if(n->val < parent->val){
      if(parent->left == NULL){
        parent->left = n;
      }else{
        addNode(parent->left, n);
      }
    }else{
      if(parent->right == NULL) {
        parent->right = n;
      }else{
        addNode(parent->right,n);
      }
    }
}
struct Queue{
    Node* start;
    Node* end;
};
typedef struct Queue Queue;
Queue* newQueue(){
    Queue * q = malloc(sizeof(Queue));
    q->start = NULL;
    q->end = NULL;
}
void addQueueNode(Queue* q, Node* n){
    if(n != NULL){
        if(q->start == NULL){
            q->start = n;
        }else{
            q->end->next = n;
        }
        q->end = n;
    }
}
void print(BinaryTree* bt){ 
    Queue q;
    q.start = NULL;
    q.end = NULL;
    addQueueNode(&q, bt->root);
    int sp = 0;
    while(q.start != NULL){
        Node* n = q.start;
        Queue aux;
        aux.start = NULL;
        aux.end = NULL;
        while(n != NULL){
            if(!sp) sp++;
            else printf(" ");
            printf("%d",n->val);
            addQueueNode(&aux,n->left);
            addQueueNode(&aux,n->right);
            n = n->next;
        }
        q = aux;
    }
    printf("\n");
}
int main(){
    int nCases;
    int i = 1;
    scanf("%d",&nCases);
    while(i <= nCases){
        int n;
        scanf("%d",&n);
        Node* root = NULL;
        while(n > 0){
            int val;
            scanf("%d",&val); 
            if(root == NULL)
              root = newNode(val);
            else
              addNode(root, newNode(val));
            n--; 
        }
        printf("Case %d:\n",i);
        BinaryTree* tree = newTree();
        tree->root = root;
        print(tree);
        printf("\n");
        i++;
    }
    return 0;
}
