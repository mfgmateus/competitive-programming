#include <stdio.h>
#include <string.h>
#include <stdlib.h>
struct Node{
    char nome[30];
    struct Node* next;
};
typedef struct Node Node;
struct Lista{
    Node* head;
    Node* amigo;
    int size;
};
typedef struct Lista Lista;
Node* newNode(char* nome){
    Node* no = malloc(sizeof(Node));
    strcpy(no->nome, nome);
    return no;
}
Lista* newLista(){
    Lista* lista = malloc(sizeof(Lista));
    lista->size = 0;
    lista->head = newNode("");
    return lista;
}
void insereHead(Lista* lista, Node* elem){
    Node* temp = lista->head;
    elem->next = temp;
    lista->head = elem;
    if(lista->amigo == NULL) lista->amigo = elem;
    lista->size++;
}
Node* getElem(Lista* lista, Node* elem){
    Node* p = lista->head;
    while(p != NULL){
        if(strcmp(p->nome, elem->nome) == 0)
            return p;
        p = p->next; 
    }
    return p;
}
void insere(Lista* lista, Node* elem){
    Node* p = lista->head;

    if(getElem(lista, elem) == NULL){
        while(p->next != NULL && strcmp(p->next->nome,elem->nome) < 0)
            p = p->next;
        elem->next = p->next;
        p->next = elem;

        if(lista->amigo == NULL || strlen(elem->nome) > strlen(lista->amigo->nome)){
            lista->amigo = elem; 
        }
    }
}
void printLista(Lista* lista){
    Node* pointer = lista->head;
    while(pointer != NULL){
        if(strcmp("",pointer->nome) != 0)
            printf("%s\n",pointer->nome);
        pointer = pointer->next; 
    }
}
int main(){
    char str[50];
    char ans[4];
    scanf("%s",str);
    Lista* lista = newLista();
    Lista* listaNaoAmigos = newLista();
    while(strcmp("FIM",str) != 0){
        scanf("%s",ans);
        if(strcmp(ans,"YES") == 0)
            insere(lista,newNode(str));
        else
            insere(listaNaoAmigos,newNode(str));
        scanf("%s",str);
    }
    printLista(lista);
    printLista(listaNaoAmigos);
    printf("\nAmigo do Habay:\n%s\n",lista->amigo->nome);
    return 0;
}
