#include <stdio.h>
/*

#Description
Faça um programa que leia um valor T e preencha um vetor N[1000] com a sequência de valores de 0 até T-1 repetidas vezes, conforme exemplo abaixo. Imprima o vetor N.

#Input
A entrada contém um valor inteiro T (2 ≤ T ≤ 50).

#Output
Para cada posição do vetor, escreva "N[i] = x", onde i é a posição do vetor e x é o valor armazenado naquela posição.

#Example
 3


------------------------------------------------------------

 N[0] = 0
 N[1] = 1
 N[2] = 2
 N[3] = 0
 N[4] = 1
 N[5] = 2
 N[6] = 0
 N[7] = 1
 N[8] = 2
 ...

*/
int main(){
	int d, i, c;
    scanf("%d",&d);
    for(i = 0; i < 1000; i++){
        if(c == d) c = 0;
        printf("N[%d] = %d\n",i,c);
        c++;
    }
	return 0;
}
