#include <stdio.h>
int shellSort(int* vet, int size);
int main(){
    int nMarmores, nConsultas;
    scanf("%d %d",&nMarmores,&nConsultas);
    int count = 1;
    while(nMarmores != 0 && nConsultas != 0){
        int marmores[nMarmores], consultas[nConsultas];
        int i = 0, j;
        while(i < nMarmores){
            scanf("%d",&marmores[i]);
            i++;
        }
        i = 0;
        while(i < nConsultas){
            scanf("%d",&consultas[i]);
            i++;
        }
        shellSort(marmores,nMarmores);          
        printf("CASE# %d:\n",count);
        for(i = 0; i < nConsultas; i++){
            int found = -1;
            for(j = 0; j <  nMarmores; j++){
                if(marmores[j] == consultas[i]){
                    found = j+1;
                    break;
                }

            }
            if(found != -1){
                printf("%d found at %d\n",consultas[i],found);
            }else{
                printf("%d not found\n",consultas[i]);
            }
        }
        count++;
        scanf("%d %d",&nMarmores,&nConsultas);
    }
    return 0;
}
int shellSort(int *vet, int size) {
    int i , j , value, vencedor;
    int gap = 1;
    while(gap < size) {
        gap = 3*gap+1;
    }
    int sum = 0;
    while ( gap > 1) {
        gap /= 3;
        for(i = gap; i < size; i++) {
            value = vet[i];
            j = i - gap;
            while (j >= 0 && value < vet[j]) {
                vet [j + gap] = vet[j];
                j -= gap;
                sum += 1;
            }
            vet [j + gap] = value;
        }
    }
    return sum;
}
