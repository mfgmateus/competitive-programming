h1,m1,h2,m2 = raw_input('').split(' ')
h1 = int(h1)
h2 = int(h2)
m1 = int(m1)
m2 = int(m2)
while h1+h2+m1+m2 <> 0:
    if h1 == 0:
        h1 = 24
    if h2 == 0:
        h2 = 24
    if h2 == h1 and m2 < m1:
        h2 += 24
    if h2 <= 12:
        h2 += 24
    if h1 <= 12:
        h1 += 24
    print ((h2*60)+m2) - ((h1*60)+m1)
    h1,m1,h2,m2 = raw_input('').split(' ')
    h1 = int(h1)
    h2 = int(h2)
    m1 = int(m1)
    m2 = int(m2)
