#include <stack>
#include <iostream>
#include <vector>

using namespace std;

bool moveVagoes(vector<char> entrada, vector<char> esperado, stack<char> estacao){
  while(!esperado.empty()){
    while(!entrada.empty()){
      estacao.push(entrada[0]);
      cout << "I";
      entrada.erase(entrada.begin());
      if(estacao.top() == esperado[0]){
        cout << "R";
        estacao.pop();
        esperado.erase(esperado.begin());
        break;
      }

    }
    while(!estacao.empty() && !esperado.empty() && estacao.top() == esperado[0]){
      cout << "R";
      estacao.pop();
      esperado.erase(esperado.begin());
    }
    if(entrada.empty() && !esperado.empty()) return false;
  }
  return true;
}

int main(){
  int n;
  while(cin >> n){
    if(n == 0) return 0;
    vector<char> esperado;
    vector<char> entrada;
    stack<char> estacao;
    for(int i = 0; i < n; i++){
      char temp;
      cin >> temp;
      entrada.push_back(temp);
    }
    for(int i = 0; i < n; i++){
      char temp;
      cin >> temp;
      esperado.push_back(temp);
    }
    if(!moveVagoes(entrada, esperado, estacao)) cout << " Impossible";
    cout << endl;
  }
  return 0;
}
