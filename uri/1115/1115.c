#include <stdio.h>
int main(){
	int a,b,i;
	scanf("%d %d", &a,&b);
	
	if(a > b){
		int temp = a;
		a = b;
		b = temp;
	}
	int s = 0;
	for(i = a; i < b; i++){
		if(i != a && i != b && (i % 2) != 0) s += i;
	}
	printf("%d\n",s);	
	return 0;
}
