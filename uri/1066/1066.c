#include <stdio.h>

int main(){
	int i, par = 0, imp = 0, pos = 0, neg = 0;
	for(i = 0; i < 5; i++){
		int d;
		scanf("%d",&d);
		if((d%2)==0) par++;
		else imp++;
		if(d < 0) neg++;
		else if(d > 0) pos++;

	}
	printf("%d valor(es) par(es)\n",par);
	printf("%d valor(es) impar(es)\n",imp);
	printf("%d valor(es) positivo(s)\n",pos);
	printf("%d valor(es) negativo(s)\n",neg);
}
