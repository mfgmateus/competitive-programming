#include <stdio.h>
int main(){
	while(1){
		int d = 0 ;
		float n,s=0;
		while(d < 2){
			scanf("%f", &n);
			if(n < 0 || n > 10) printf("nota invalida\n");
			else {
				s += n;
				d++;		
			}
		}
		printf("media = %.2f\n",s/2.0);
		printf("novo calculo (1-sim 2-nao)\n");
		scanf("%d",&d);
		while(d != 1 && d != 2){
			printf("novo calculo (1-sim 2-nao)\n");
			scanf("%d",&d);
		}
		if(d == 2) return 0;
	}
	return 0;
}
