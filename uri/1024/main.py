i = input()
def increaseTreeIfAlpha(string):
    newString = ''
    for ch in string:
        if ch.isalpha():
            ch = chr(ord(ch) + 3)
        newString += ch
    return newString
def increaseOneAfterHalf(string):
    newString = ''
    size = len(string)
    half = int(size/2)
    for i in range(0,size):
        ch = string[i]
        if i >= half:
            ch = chr(ord(ch) - 1)
        newString += ch
    return newString
while i:
    string = raw_input('')
    string = increaseTreeIfAlpha(string)
    string = string[::-1]
    string = increaseOneAfterHalf(string)
    print string
    i -= 1;
