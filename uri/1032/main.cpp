#include <iostream>
#include <vector>
#include <math.h>
int isPrime(int value){
	if(value > 1){
		for(int k = 2; k <= sqrt(value); k++){
			if((value % k) == 0)
				return 0;
		}
		return 1;
	}
	return 0;
}
int nextPrime(int currentPrime){
	int n = currentPrime + 1;
	while(!isPrime(n)){
		n++;
	}
	return n;
}
int main(){
	int n,i,c; 
    std::cin >> n;
	while(n != 0){
		std::vector<int> p;
        for(int i = 0; i < n; i++){
            p.push_back(i+1);
        }
        int prime = 2;
        int r = 0;
        while(p.size() > 1){
            r = (r + prime - 1) % p.size(); 
            p.erase(p.begin()+r);
            prime = nextPrime(prime);
        }
        std::cout << p[0];
        std::cout << '\n';
        std::cin >> n;
	}
	return 0;
}   
