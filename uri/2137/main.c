#include <stdio.h>
int main(){
    int cases;
    while(scanf("%d",&cases) != EOF){
        int numbers[cases];
        int i = cases, j;
        while(i > 0){
            scanf("%d",&numbers[i-1]);
            i--;
        }
        for(i = 0; i < cases; i++){
            for(j = 0; j < cases; j++){
                if(numbers[i] < numbers[j]){
                    int temp = numbers[i];
                    numbers[i] = numbers[j];
                    numbers[j] = temp;
                }
            }
        }
        for(i = 0; i < cases; i++)
            printf("%04d\n",numbers[i]);
    }
    return 0;
}
