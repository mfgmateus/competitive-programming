#include <stdio.h>
int main(){
	int integer, decimal;
	scanf("%d.%d",&integer,&decimal);
	printf("NOTAS:\n");
	printf("%d nota(s) de R$ 100.00\n",(integer / 100));
	integer = (integer % 100);
	printf("%d nota(s) de R$ 50.00\n",(integer / 50));
	integer = (integer % 50);
	printf("%d nota(s) de R$ 20.00\n",(integer / 20));
	integer = (integer % 20);
	printf("%d nota(s) de R$ 10.00\n",(integer / 10));
	integer = (integer % 10);
	printf("%d nota(s) de R$ 5.00\n",(integer / 5));
	integer = (integer % 5);
	printf("%d nota(s) de R$ 2.00\n",(integer / 2));
	integer = (integer % 2);
	printf("MOEDAS:\n");
	printf("%d moeda(s) de R$ 1.00\n",(integer));
	printf("%d moeda(s) de R$ 0.50\n",(decimal / 50));
	decimal = (decimal % 50);
	printf("%d moeda(s) de R$ 0.25\n",(decimal / 25));
	decimal = (decimal % 25);
	printf("%d moeda(s) de R$ 0.10\n",(decimal / 10));
	decimal = (decimal % 10);
	printf("%d moeda(s) de R$ 0.05\n",(decimal / 5));
	decimal = (decimal % 5);
	printf("%d moeda(s) de R$ 0.01\n",(decimal));
	return 0;
}
