/**

https://www.urionlinejudge.com.br/judge/pt/problems/view/1011

**/

#include <stdio.h>

int main(){
	int R;
	float pi = 3.14159;
	scanf("%d",&R);
	printf("VOLUME = %.3lf\n",((4.0/3.0)*3.14159*(R*R*R)));
	return 0;
}




