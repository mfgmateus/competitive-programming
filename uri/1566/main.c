#include <stdio.h>
int main(){
    int cases = 0;
    scanf("%d",&cases);
    while(cases > 0){
        int n = 0, i;
        scanf("%d",&n);
        int size[231]; 
        for(i = 0; i <= 231; i++){
            size[i] = 0;
        }
        while(n > 0){
            int val;
            scanf("%d",&val);
            size[val]++;
            n--;
            if(n == 0){ 
                int sp = 0;
                for(i = 0; i <= 231; i++){
                    int j = 0;
                    for(j = 0; j < size[i]; j++){
                        if(sp) printf(" ");
                        else sp++;
                        printf("%d",i);
                    }       
                }
                printf("\n");
            }
        }
        cases--;
    }
    return 0;
}
