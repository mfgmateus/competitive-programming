#include <stdio.h>

int main(){
	int a,b,sum=0;
	scanf("%d\n%d",&a,&b);
	if(a > b){
		int temp = a;
		a = b;
		b = temp;
	}
	if(a >0){
		a--;
	}else{
		a++;
	}
	while(a < b){
		if((a % 2) != 0) sum += a;		
		a++;
	}
	printf("%d\n",sum);
}
