#include <stdio.h>
int shellSort(int* vet, int size);
int main(){
    int cases = 0;
    while(scanf("%d",&cases) != EOF){
        if(cases ==  0) break;
        int i = cases, j;
        int arr[i];
        int vencedor = 0;
        for(i = 0; i < cases; i++){
            scanf("%d",&arr[i]); 
        }
        vencedor = shellSort(arr,cases);
        if((vencedor % 2) != 0) printf("Marcelo\n");
        else printf("Carlos\n");
    }
    return 0;
}
int shellSort(int *vet, int size) {
    int i , j , value, vencedor;
    int gap = 1;
    while(gap < size) {
        gap = 3*gap+1;
    }
    int sum = 0;
    while ( gap > 1) {
        gap /= 3;
        for(i = gap; i < size; i++) {
            value = vet[i];
            j = i - gap;
            while (j >= 0 && value < vet[j]) {
                vet [j + gap] = vet[j];
                j -= gap;
                sum += 1;
            }
            vet [j + gap] = value;
        }
    }
    return sum;
}
