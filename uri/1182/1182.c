#include <stdio.h>
/*

#Description
Neste problema você deve ler um número que indica uma coluna de uma matriz na qual uma operação deve ser realizada, um caractere maiúsculo, indicando a operação que será realizada, e todos os elementos de uma matriz M[12][12]. Em seguida, calcule e mostre a soma ou a média dos elementos que estão na área verde da matriz, conforme for o caso. A imagem abaixo ilustra o caso da entrada do valor 5 para a coluna da matriz, demonstrando os elementos que deverão ser considerados na operação.

#Input
A primeira linha de entrada contem um número C (0 ≤ C ≤ 11) indicando a coluna que será considerada para operação. A segunda linha de entrada contém um único caractere Maiúsculo T ('S' ou 'M'), indicando a operação (Soma ou Média) que deverá ser realizada com os elementos da matriz. Seguem os 144 valores de ponto flutuante que compõem a matriz.

#Output
Imprima o resultado solicitado (a soma ou média), com 1 casa após o ponto decimal.

#Example
 5
 S
 0.0
 -3.5
 2.5
 4.1
 ...


------------------------------------------------------------

 12.6

*/
#include <stdio.h>
 
//Nao usei matriz :)
 int main(){
     float s=0,v;
     int i,j,d;
     char c;
     scanf("%d\n%c",&d,&c);
     for(i = 0; i < 12; i++){
         for(j = 0; j < 12; j++){
             scanf("%f",&v);
             if(j == d)
                 s+= v;
         }
     }
     if(c == 'M')
         s = s/12.0;
 
     printf("%.1f\n",s);
 
     return 0;
 }
