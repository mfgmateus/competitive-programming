#include <stdio.h>
/*

#Description
Faça um programa que leia um valor e apresente o número de Fibonacci correspondente a este valor lido. Lembre que os 2 primeiros elementos da série de Fibonacci são 0 e 1 e cada próximo termo é a soma dos 2 anteriores a ele. Todos os valores de Fibonacci calculados neste problema devem caber em um inteiro de 64 bits sem sinal.

#Input
A primeira linha da entrada contém um inteiro T, indicando o número de casos de teste. Cada caso de teste contém um único inteiro N (0 ≤ N ≤ 60), correspondente ao N-esimo termo da série de Fibonacci.

#Output
Para cada caso de teste da entrada, imprima a mensagem "Fib(N) = X", onde X é o N-ésimo termo da série de Fibonacci.

#Example
 3
 0
 4
 2


------------------------------------------------------------

 Fib(0) = 0
 Fib(4) = 3
 Fib(2) = 1

*/
int main(){
	int i=0,d = 0;
    scanf("%d",&i);
    while(i >0){
        scanf("%d",&d);
        int ind, fib=0;
        int v1 = 0;
        int v2 = 0;
        for(ind = 0; ind < d; ind++){
            if(ind <= 1)
              v2 = ind;
            fib = v1 + v2;
            v1 = v2;
            v2 = fib;
        }
        i--;
        printf("Fib(%d) = %d\n",d,fib);
    }
	return 0;
}
