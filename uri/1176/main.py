fibs = [0,1]
for i in range(2,61):
    fibs.append(fibs[i-1]+fibs[i-2])
cases = input()
while cases > 0:
    n = int(input())
    print "Fib(%d) = %d" % (n,fibs[n])
    cases -= 1
