#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*

#Description
Faça um programa que leia um valor N. Este N será o tamanho de um vetor X[N]. A seguir, leia cada um dos valores de X, encontre o menor elemento deste vetor e a sua posição dentro do vetor, mostrando esta informação.

#Input
A primeira linha de entrada contem um único inteiro N (1 < N < 1000), indicando o número de elementos que deverão ser lidos em seguida para o vetor X[N] de inteiros. A segunda linha contém cada um dos N valores, separados por um espaço.

#Output
A primeira linha apresenta a mensagem “Menor valor:” seguida de um espaço e do menor valor lido na entrada. A segunda linha apresenta a mensagem “Posicao:” seguido de um espaço e da posição do vetor na qual se encontra o menor valor lido, lembrando que o vetor inicia na posição zero.

#Example
 10
 1 2 3 4 -5 6 7 8 9 10


------------------------------------------------------------

 Menor valor: -5
 Posicao: 4

*/
int main(){
    int s,iM=0,vM,i=0,c=0;
    char str[100000];
	scanf("%d",&s);
    fgets (str, 100000, stdin);
    char n[1000];
    strcpy(n,"");
    printf("%s\n",str);
    while(1){
        if(str[i] == ' ' || str[i] == '\0'){
            if(c == 0)
                vM = atoi(n);
            else{
                printf("%s\n",n);
                if(vM > atoi(n)){
                    vM = atoi(n);
                    iM = c;
                }
            }
            strcpy(n,"");
            c++;
            if(str[i] == '\0') break;
        }
        else{
            char v[] = "0";
            if(str[i] != ' ') printf("%c\n",str[i]);
            v[0] = str[i];
            strcat(n,v);
        }
        i++;
    }
    printf("Menor valor: %d\n",vM);
    printf("Posicao: %d\n",iM);

	return 0;
}
