#include <stdio.h>
void shellSort(int *vet, int size);
int main(){
    int cases = 0;
    scanf("%d",&cases);
    int impares[cases];
    int pares[cases];
    int indP = 0, indI = 0;
    while(cases > 0){
        int n;
        scanf("%d",&n);
        if(n%2==0){
            pares[indP] = n;
            indP++;
        }else{
            impares[indI] = n;
            indI++;
        }
        cases--;
    }
    int n = 0;
        shellSort(pares, indP);
        shellSort(impares, indI);
        for(n = 0; n < indP; n++){
            printf("%d\n",pares[n]);
        }
        for(n = indI-1; n >= 0; n--){
            printf("%d\n",impares[n]);
        }
    return 0;
}
void shellSort(int *vet, int size) {
    int i , j , value, vencedor;
    int gap = 1;
    while(gap < size) {
        gap = 3*gap+1;
    }
    int sum = 0;
    while ( gap > 1) {
        gap /= 3;
        for(i = gap; i < size; i++) {
            value = vet[i];
            j = i - gap;
            while (j >= 0 && value < vet[j]) {
                vet [j + gap] = vet[j];
                j -= gap;
            }
            vet [j + gap] = value;
        }
    }
}
