#include <stack>
#include <iostream>
#include <vector>

using namespace std;

bool moveVagoes(vector<int> entrada, vector<int> esperado, stack<int> estacao){
    while(!esperado.empty()){
        while(!entrada.empty()){
            estacao.push(entrada[0]);
            //cout << "Insere " << entrada[0] << endl;
            entrada.erase(entrada.begin());
            if(estacao.top() == esperado[0]){
                //cout << "Remove " << estacao.top() << endl;
                estacao.pop();
                esperado.erase(esperado.begin());
                break;
            }

        }
        while(!estacao.empty() && !esperado.empty() && estacao.top() == esperado[0]){
            //cout << "Remove " << estacao.top() << endl;
            estacao.pop();
            esperado.erase(esperado.begin());
        }
        if(entrada.empty() && !esperado.empty()) return false;
    }
    return true;
}

int main(){
    int n;
    while(cin >> n){
        if(n == 0) return 0;
        int k = n;
        bool c = true;
        while(c){
            vector<int> esperado;
            vector<int> entrada;
            stack<int> estacao;
            for(int i = 0; i < n; i++){
                int temp;
                cin >> temp;
                if(temp == 0) {
                    c = false;
                    break;      
                }
                //cout << temp << " ";
                entrada.push_back(i+1);
                esperado.push_back(temp);
            }
            //cout << endl;
            if(!c) break;
            if(moveVagoes(entrada, esperado, estacao)) cout << "Yes";
            else cout << "No";
            cout << endl;
        }
        cout << endl;
    }
    return 0;
}
