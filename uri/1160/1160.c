#include <stdio.h>
int main(){
	int d,i;
	scanf("%d",&d);
	while(d > 0){
		double c1,t1,c2,t2;
		int t;
		scanf("%lf %lf %lf %lf",&c1,&c2,&t1,&t2);
		while(c1 <= c2){
			printf("c1=%lf,c2=%lf,t=%d\n",c1,c2,t);
			c1 += (int)((double)c1*(t1/100.0));
			c2 += (int)((double)c2*(t2/100.0));
			t++;
		}
		if(t > 100) printf("Mais de 1 seculo.\n");
		else printf("%d anos.\n",t);
		d--;
	}
	return 0;
}
