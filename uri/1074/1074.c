#include <stdio.h>

int main(){
	int d,i;
	scanf("%d",&d);
	int l[d];
	for(i = 0; i < d; i++){
		scanf("%d",&l[i]);
	}
	for(i = 0; i < d; i++){
		if(l[i] == 0) printf("NULL\n");
		else if(l[i] > 0 && (l[i] %2) == 0) printf("EVEN POSITIVE\n");
		else if(l[i] < 0 && (l[i] %2) == 0) printf("EVEN NEGATIVE\n");
		else if(l[i] > 0 && (l[i] %2) != 0) printf("ODD POSITIVE\n");
		else if(l[i] < 0 && (l[i] %2) != 0) printf("ODD NEGATIVE\n");	
	}
}

