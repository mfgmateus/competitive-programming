#include <stdio.h>

int main(){
	float s, imp = 0;
	scanf("%f",&s);
	if(s <= 2000) {printf("Isento\n"); return 0;}
	if(s <= 3000) imp = (s-2000) * 0.08;
	else if(imp <= 4500) imp = (80) + (s-3000)*0.18;	
	imp = (s > 4500) ? 80 + 270 + ((s-4500)*0.28) : imp;
	printf("R$ %.2f\n",imp);
	return 0;
}
