#include <iostream>
#include <stack>

using namespace std;

int precedencia(char a){
  if(a == '(')
    return 4;
  if(a == '^')
    return 0;
  if(a == '*' || a == '/')
    return 1;
  return 2;
}
bool isOperador(char a){
  if(a == '+' || a == '-' || a == '*' || a == '/' || a == '^')
    return true;
  return false;
}
bool isOperando(char a){
  if(!isOperador(a) && a != ')' && a != '(')
    return true;
  return false;
}
int main(){
  int n;
  cin >> n;
  while(n > 0){
    stack<char> s;
    string expressao;
    cin >> expressao;
    stack<char> pilha;
    for(int i = 0; i < expressao.length(); i++){
      char c = expressao.at(i);
      if(isOperando(c))
        cout << c;
      else if (c == '(')
        pilha.push(c);
      else if(c == ')'){
        while(pilha.top() != '('){
          cout << pilha.top();
          pilha.pop();
        }
        pilha.pop();
      }
      else{
        while(!pilha.empty() && (precedencia(c) >= precedencia(pilha.top()))){
          cout << pilha.top();
          pilha.pop();
        }
        pilha.push(c);
      }
    }
    while(!pilha.empty()){
      cout << pilha.top();
      pilha.pop();
    }
    cout << endl;
    n--;
  }
}

